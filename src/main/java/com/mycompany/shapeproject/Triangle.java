/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author 66955
 */
public class Triangle extends Shape{
        
    private double base;
    private double hight;

    public Triangle(double base,double hight) {
        super("Triangle");
        this.base = base;
        this.hight = hight;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHight() {
        return hight;
    }

    public void setHight(double hight) {
        this.hight = hight;
    }

    
    @Override
    public double calArea() {
        return 0.5*base*hight;
    }

    @Override
    public double calPerimeter() {
        double c=0;
        c = Math.pow(hight, 2) + Math.pow(base, 2);
        return (Math.sqrt(c))+hight+base;
    }

}
