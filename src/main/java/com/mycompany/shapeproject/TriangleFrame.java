/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author 66955
 */
public class TriangleFrame {
        public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblbase = new JLabel("base", JLabel.TRAILING);
        lblbase.setSize(50, 20);
        lblbase.setLocation(5, 5);
        lblbase.setBackground(Color.white);
        lblbase.setOpaque(true);
        frame.add(lblbase);
        
        JLabel lblhight = new JLabel("hight", JLabel.TRAILING);
        lblhight.setSize(50, 20);
        lblhight.setLocation(5, 20);
        lblhight.setBackground(Color.white);
        lblhight.setOpaque(true);
        frame.add(lblhight);

        final JTextField txtbase = new JTextField();
        txtbase.setSize(50, 20);
        txtbase.setLocation(60, 5);
        frame.add(txtbase);
        
        final JTextField txthight = new JTextField();
        txthight.setSize(50, 20);
        txthight.setLocation(60, 20);
        frame.add(txthight);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 10);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Triangle =??? area = ??? perimeter=???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strbase = txtbase.getText();
                String strhight = txthight.getText();
                double base = Double.parseDouble(strbase);
                double hight = Double.parseDouble(strhight);
                Triangle triangle = new Triangle(base,hight);
                lblResult.setText("Triangle base = " + String.format("%.2f", triangle.getBase())
                        + " hight = "+String.format("%.2f", triangle.getHight())
                        + " area = " + String.format("%.2f", triangle.calArea())
                        + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame,"Error: Please input number","Error",JOptionPane.ERROR_MESSAGE);
                    txtbase.setText("");
                    txthight.setText("");
                    txtbase.requestFocus();
                    txthight.requestFocus();
                }
            }
        });

        frame.setVisible(true);

    }
}
