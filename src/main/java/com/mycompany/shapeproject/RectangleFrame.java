/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author 66955
 */
public class RectangleFrame {
        public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblwide = new JLabel("wide", JLabel.TRAILING);
        lblwide.setSize(50, 20);
        lblwide.setLocation(5, 5);
        lblwide.setBackground(Color.white);
        lblwide.setOpaque(true);
        frame.add(lblwide);
        
        JLabel lbllongg = new JLabel("long", JLabel.TRAILING);
        lbllongg.setSize(50, 20);
        lbllongg.setLocation(5, 20);
        lbllongg.setBackground(Color.white);
        lbllongg.setOpaque(true);
        frame.add(lbllongg);

        final JTextField txtwide = new JTextField();
        txtwide.setSize(50, 20);
        txtwide.setLocation(60, 5);
        frame.add(txtwide);
        
        final JTextField txtlongg = new JTextField();
        txtlongg.setSize(50, 20);
        txtlongg.setLocation(60, 20);
        frame.add(txtlongg);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 10);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle =??? area = ??? perimeter=???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(500, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
        
        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                String strwide = txtwide.getText();
                String strlongg = txtlongg.getText();
                double wide = Double.parseDouble(strwide);
                double longg = Double.parseDouble(strlongg);
                Rectangle rectangle = new Rectangle(wide,longg);
                lblResult.setText("Rectangle wide = " + String.format("%.2f", rectangle.getWide())
                        + " long = "+String.format("%.2f", rectangle.getLongg())
                        + " area = " + String.format("%.2f", rectangle.calArea())
                        + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(frame,"Error: Please input number","Error",JOptionPane.ERROR_MESSAGE);
                    txtwide.setText("");
                    txtlongg.setText("");
                    txtwide.requestFocus();
                    txtlongg.requestFocus();
                }
            }
        });



        frame.setVisible(true);

    }
}
