/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author 66955
 */
public class Rectangle extends Shape{
    
    private double wide;
    private double longg;

    public Rectangle(double wide,double longg) {
        super("Rectangle");
        this.wide = wide;
        this.longg = longg;
    }

    public double getWide() {
        return wide;
    }

    public void setWide(double wide) {
        this.wide = wide;
    }

    public double getLongg() {
        return longg;
    }

    public void setLongg(double longg) {
        this.longg = longg;
    }

    
    @Override
    public double calArea() {
        return wide*longg;
    }

    @Override
    public double calPerimeter() {
        return (2*wide)+(2*longg);
    }
}
